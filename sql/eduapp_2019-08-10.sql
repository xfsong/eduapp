# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: test.me (MySQL 5.6.38-log)
# Database: eduapp
# Generation Time: 2019-08-10 11:54:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table babys
# ------------------------------------------------------------

DROP TABLE IF EXISTS `babys`;

CREATE TABLE `babys` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `memid` int(11) NOT NULL DEFAULT '0' COMMENT '会员id',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `gender` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0:女,1:男',
  `logos` varchar(500) NOT NULL DEFAULT '' COMMENT '图片',
  `birthday` int(11) NOT NULL DEFAULT '0' COMMENT '生日',
  `cretime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table baike
# ------------------------------------------------------------

DROP TABLE IF EXISTS `baike`;

CREATE TABLE `baike` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(300) NOT NULL DEFAULT '' COMMENT '标题',
  `summary` int(11) NOT NULL COMMENT '摘要',
  `logos` varchar(500) NOT NULL DEFAULT '' COMMENT '图片',
  `catid` int(11) NOT NULL DEFAULT '0' COMMENT '分类ID',
  `contents` text COMMENT '内容',
  `memid` int(11) NOT NULL COMMENT '作者',
  `clicks` int(11) NOT NULL DEFAULT '0' COMMENT '点击数量',
  `likes` int(11) NOT NULL DEFAULT '0' COMMENT '被喜欢数量',
  `cretime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table baike_cate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `baike_cate`;

CREATE TABLE `baike_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `cretime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table collects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `collects`;

CREATE TABLE `collects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `memid` int(11) NOT NULL DEFAULT '0' COMMENT '会员ID',
  `object_id` int(11) NOT NULL DEFAULT '0' COMMENT '收藏对象ID',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '1:百科,2:菜谱',
  `cretime` int(11) NOT NULL DEFAULT '0' COMMENT '收藏时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table food_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `food_menu`;

CREATE TABLE `food_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(300) NOT NULL DEFAULT '' COMMENT '标题',
  `summary` int(11) NOT NULL COMMENT '摘要',
  `logos` varchar(500) NOT NULL DEFAULT '' COMMENT '图片',
  `catid` int(11) NOT NULL DEFAULT '0' COMMENT '分类ID',
  `contents` text COMMENT '内容',
  `memid` int(11) NOT NULL COMMENT '作者',
  `clicks` int(11) NOT NULL DEFAULT '0' COMMENT '点击数量',
  `likes` int(11) NOT NULL DEFAULT '0' COMMENT '被喜欢数量',
  `cretime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table foods_cate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `foods_cate`;

CREATE TABLE `foods_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `cretime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table grow_notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `grow_notes`;

CREATE TABLE `grow_notes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `memid` int(11) NOT NULL DEFAULT '0' COMMENT '会员ID',
  `babyid` int(11) NOT NULL DEFAULT '0' COMMENT '宝宝ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `contents` text COMMENT '内容',
  `cretime` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  `body_tall` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '身高',
  `body_heavy` decimal(11,2) DEFAULT NULL COMMENT '体重',
  `birthday` int(11) NOT NULL DEFAULT '0' COMMENT '出生日期',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;



# Dump of table grow_records
# ------------------------------------------------------------

DROP TABLE IF EXISTS `grow_records`;

CREATE TABLE `grow_records` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `memid` int(11) NOT NULL DEFAULT '0' COMMENT '会员ID',
  `babyid` int(11) NOT NULL DEFAULT '0' COMMENT '宝宝ID',
  `cretime` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  `body_tall` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '身高',
  `body_heavy` decimal(11,2) DEFAULT NULL COMMENT '体重',
  `birthday` int(11) NOT NULL DEFAULT '0' COMMENT '出生日期',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;



# Dump of table members
# ------------------------------------------------------------

DROP TABLE IF EXISTS `members`;

CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wx` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qq` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `members_name_unique` (`name`),
  UNIQUE KEY `members_mobile_unique` (`mobile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table message
# ------------------------------------------------------------

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '内容',
  `cretime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `isread` int(11) NOT NULL DEFAULT '0' COMMENT '0:未读,1:已读',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2019_08_10_080435_create_members_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table vaccinate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vaccinate`;

CREATE TABLE `vaccinate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `memid` int(11) NOT NULL DEFAULT '0' COMMENT '会员ID',
  `babyid` int(11) NOT NULL DEFAULT '0' COMMENT '宝宝ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
