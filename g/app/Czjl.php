<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Czjl extends Model
{
    protected $table='czjl';
    protected $fillable=[
        'cm','kg','date'
    ];
}
