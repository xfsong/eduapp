<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Babys extends Model
{
    protected $table='babys';
    protected $fillable=['memid','nickname','gender','logos','birthday'];
}
