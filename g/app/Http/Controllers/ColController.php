<?php

namespace App\Http\Controllers;

use App\Col;
use Illuminate\Http\Request;

class ColController extends Controller
{
    //
    public function show()
    {
//        数据查询数据
        $data = Col::all();
        return json_encode(['code' => '200', 'msg' => '展示成功', 'data' => $data]);
//        http://127.0.0.1/training-two/baidu-collect/public/api/show
    }

    public function part()
    {
//        数据详情查询
        $id=$_REQUEST['id'];
//        单条数据查询
        $data=Col::findOrFail($id);
        return json_encode(['code' => '200', 'msg' => '展示成功', 'data' => $data]);
//        http://127.0.0.1/training-two/baidu-collect/public/api/part?id=1
    }
}
