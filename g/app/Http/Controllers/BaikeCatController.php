<?php

namespace App\Http\Controllers;

use App\Baike;
use App\BaikeCat;
use Illuminate\Http\Request;

class BaikeCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
	    $page = $request->input('page', 1);
	    $pageSize = 5;
	    $offset = ($page - 1) * $pageSize;
	    $data['data'] = BaikeCat::where([])->offset($offset)->limit($pageSize)->get();
	    $data['total_page'] = BaikeCat::where([])->count();
	    if($data) {
		    return [
			    'code'    => 200,
			    'message' => 'ok',
			    'data'    => $data
		    ];
	    } else {
		    return [
			    'code'    => 200,
			    'message' => 'ok',
			    'data'    => []
		    ];
	    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
	    $validator = Validator::make($request->all(), [
		    'title' => 'required|unique:baike_cat',
	    ]);
	    if($validator->fails()) {
		    return $this->jsonRes(0, "请检查传入参数");
	    }
	    //验证重复
	    $baikeCatData = BaikeCat::where('title', '=', $request->input('title'))->get(array('id'))->toArray();
	    if(!empty($baikeCatData)) {
		    return $this->jsonRes(0, "不能重复");
	    }

	    $baikeCatDatas = BaikeCat::create($request->all());
	    return $this->jsonRes(1, $baikeCatDatas);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BaikeCat  $baikeCat
     * @return \Illuminate\Http\Response
     */
    public function show(BaikeCat $baikeCat)
    {
        //
	    return $this->jsonRes(1, $baikeCat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BaikeCat  $baikeCat
     * @return \Illuminate\Http\Response
     */
    public function edit(BaikeCat $baikeCat)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BaikeCat  $baikeCat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BaikeCat $baikeCat)
    {
        //
	    $baikeCat->update($request->all());
	    return $this->jsonRes(1, $baikeCat);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BaikeCat  $baikeCat
     * @return \Illuminate\Http\Response
     */
    public function destroy(BaikeCat $baikeCat)
    {
        //
    }
}
