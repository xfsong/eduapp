<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Babys;
use App\Grow_records;
class BabyColltroller extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //查询一条
    public function show($id)
    {
    	if (is_numeric($id)) 
    	{
       		 return json_encode(['msg'=>'请检查id是否正确','code'=>4003]);	
    	}
    	
         return json_encode(['code'=>'2003','msg'=>'查询成功','data'=>DB::table('babys')
        ->join('grow_records', 'babys.id', '=', 'grow_records.babyid')
        ->where('babys.id',$id)
        ->get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //修改
   public function upload(Request $request)
    {
     header('Access-Control-Allow-Origin:*');
     header('Access-Control-Allow-Methods:*');
     header('Access-Control-Allow-Headers:*');
     header('Access-Control-Allow-Credentials:false');
     // $data=$_POST;
     // return $data;
     if ($request->isMethod('POST')){
         $file = $request->file('userfile');
         //判断文件是否上传成功
         if ($file->isValid()){
             //原文件名
             $originalName = $file->getClientOriginalName();
             //扩展名
             $ext = $file->getClientOriginalExtension();
             //MimeType
             $type = $file->getClientMimeType();
             //临时绝对路径
             $realPath = $file->getRealPath();
             $filename = uniqid().'.'.$ext;

             move_uploaded_file($realPath,"../storage/".$filename);
             $img=asset('storage/'.$filename);

             // $data=new Users();
             // $data->img=$img;
         
             
             // $data->save();


             if ($img) 
             {
                 return json_encode(['code'=>2001,'msg'=>'上传成功','data'=>$img]);
             }
             else
             {
                 return json_encode(['code'=>4001,'msg'=>'上传失败','data'=>'无']);
             }
             
            }
          }

    }
    /**
     *文件上传
     */
    public function upload(Request $request)
    {

        $path = $request->file('avatar')->store('avatars');
          
        return $this->jsonRes(0,$path);
       
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 验证
        $validator = Validator::make($request->all(), [
            'memid' => 'required|integer',
            'nickname' => 'required|min:3|max:6',
            'gender' => 'required|numeric',
            'birthday' => 'required',
            'logos' => 'required|image'
        ]);

        if ($validator->fails()) {
            return $this->jsonRes(1,'请重新输入参数');
        }
        
        $insertData = [
            "memid" => $request->input("memid"),
            "nickname" => $request->input("nickname"),
            "gender" => $request->input('gender'),
            "logos" => $request->input('img'),
            "birthday" => $request->input("birthday")
        ];
        // 存入数据库
        Babys::create($insertData);
        return $this->jsonRes(0,'添加成功');
    }
}