<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grow_records;
use Validator;

class ExamController extends Controller
{
    /**
     * 添加身高和体重.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function growEdit(Request $request){

        header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:*');
		header('Access-Control-Allow-Headers:*');
        header('Access-Control-Allow-Credentials:false');

        $validator = Validator::make($request->all(), [
            'body_tall' => 'required|numeric|max:255',
            'body_heavy' => 'required|numeric|max:255',
            'cretime' => 'required',
        ]);

        if ($validator->fails()) {
            return ['status'=>'0','result'=>'参数格式错误'];
        }

        $res = Grow_records::create([
            'memid'=>$request->memid ?? 0,
            'babyid'=>$request->babyid ?? 0,
            'body_tall'=>$request->body_tall,
            'body_heavy'=>$request->body_heavy,
            'cretime'=>$request->cretime,
        ]);

        if($res){
            return ['status'=>'1','result'=>'添加成功'];
        }else{
            return ['status'=>'0','result'=>'添加失败'];
        }
    }

    //
    /**
     * 查询身高和体重
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function growShow(Request $request){

        header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:*');
		header('Access-Control-Allow-Headers:*');
        header('Access-Control-Allow-Credentials:false');
        
        $growShow = Grow_records::where('babyid',$request->babyid)->select('body_tall','body_heavy')->get();
        if($growShow){
            return ['status'=>'1','result'=>$growShow];
        }else{
            return ['status'=>'0','result'=>''];
        }
    }
}
