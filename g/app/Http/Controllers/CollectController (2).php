<?php

namespace App\Http\Controllers;

use App\Collect;
use App\Login;
use Illuminate\Http\Request;
use http\Env\Response;


class CollectController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * 收藏列表
     */
    public function show(Request $request){
        $page = $request -> input('page',1);//获取页码
        $user_id = $request -> input('user_id');//获取用户id
        $pagesize = '5';//显示条数
        $offset = ($page-1)*$pagesize;//偏移量
        $data['data'] = Collect::where(['user_id'=>$user_id])
            ->offset($offset)
            ->limit($pagesize)
            ->get();
        $data['total_page'] = Collect::count();//总条数
        return Response()->json(['code'=>2000,'msg'=>'请求成功','data'=>$data]);
    }

    /**
     * @param Request $request
     * @return mixed
     * 收藏详情
     */
    public function info(Request $request){
        $id = $request->input('id');//获取id
        $data = Collect::findorfail($id);//获取数据
        return Response()->json(['code'=>2000,'msg'=>'请求成功','data'=>$data]);
    }


}