<?php

namespace App\Http\Controllers;

use App\Baby;
use Illuminate\Http\Request;

class BabyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
	    $page = $request->input('page', 1);
	    $memid = $request->input('memid', 0);
	    $pageSize = 5;
	    $offset = ($page - 1) * $pageSize;
	    $data['data'] = Baby::where(["memid" => $memid])->offset($offset)->limit($pageSize)->get();
	    $data['total_page'] = Baby::where([])->count();
	    if($data) {
		    return [
			    'code'    => 200,
			    'message' => 'ok',
			    'data'    => $data
		    ];
	    } else {
		    return [
			    'code'    => 200,
			    'message' => 'ok',
			    'data'    => []
		    ];
	    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
	    $validator = Validator::make($request->all(), [
		    'nickname' => 'required|unique:baby'
	    ]);
	    if($validator->fails()) {
		    return $this->jsonRes(0, "请检查传入参数");
	    }
	    //验证重复
	    /*$babyDatas = Baby::where('nickname', '=', $request->input('nickname'))->get(array('id'))->toArray();
	    if(!empty($babyDatas)) {
		    return $this->jsonRes(0, "不能重复");
	    }*/

	    $babyDatas = Baby::create($request->all());
	    return $this->jsonRes(1, $babyDatas);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Baby  $baby
     * @return \Illuminate\Http\Response
     */
    public function show(Baby $baby)
    {
        //
	    return $this->jsonRes(1, $baby);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Baby  $baby
     * @return \Illuminate\Http\Response
     */
    public function edit(Baby $baby)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Baby  $baby
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Baby $baby)
    {
        //

	    $baby->update($request->all());
	    return $this->jsonRes(1, $baby);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Baby  $baby
     * @return \Illuminate\Http\Response
     */
    public function destroy(Baby $baby)
    {
        //
    }
}
