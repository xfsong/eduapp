<?php

namespace App\Http\Controllers;

use App\Collect;
use App\Login;
use Illuminate\Http\Request;

class CollectController extends Controller
{
	//对接口的封装
    public function initjson($code,$msg,$data){
        return json_encode(['code'=>$code,'msg'=>$msg,'data'=>$data]);
    }
    //收藏展示
    public function show(){
    	header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:*');
		header('Access-Control-Allow-Headers:*');
		header('Access-Control-Allow-Credentials:false');
        $data = Collect::all();
        return $this->initjson(200,'请求成功',$data);
    }
    //收藏详情
    public function info(){
    	header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:*');
		header('Access-Control-Allow-Headers:*');
		header('Access-Control-Allow-Credentials:false');
        $id = $_GET['id'];
        $data = Collect::findorfail($id);
        return $this->initjson(200,'请求成功',$data);
    }
    //登录接口
    public function login(Request $request){
    	header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:*');
		header('Access-Control-Allow-Headers:*');
		header('Access-Control-Allow-Credentials:false');
		 $this->validate($request, [
        'name' => 'min:2|max:10',
        'password' => 'min:6|max:16',
    	]);
        $res = Login::where(['name'=>$request->name,'password'=>$request->password])->first();
        if($res){
            return $this->initjson(200,'登录成功',[]);
        }else{
            return $this->initjson(100,'登录失败',[]);
        }
    }

}