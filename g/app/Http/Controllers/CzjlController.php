<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Czjl;
use Validator;

class CzjlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        header('Access-Control-Allow-Origin:*');
        $data=Czjl::all();
        if($data)
        {
            return json_encode([
                'code'=>0,
                'msg'=>'ok',
                'data'=>$data
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        header('Access-Control-Allow-Origin:*');

        $yz_cm=Validator::make($request->all(),[
            'cm'=>['required','regex:/^\d+$/']
        ]);
        if($yz_cm->fails())
        {
            return json_encode([
                'code'=>2,
                'msg'=>'身高为数字,请填写正确身高',
                'data'=>false
            ]);
        }

        $yz_kg=Validator::make($request->all(),[
            'kg'=>['required','regex:/^\d+$/']
        ]);
        if($yz_kg->fails())
        {
            return json_encode([
                'code'=>3,
                'msg'=>'体重为数字,请填写正确体重',
                'data'=>false
            ]);
        }

        $res=Czjl::create($request->all());
        if($res)
        {
            return json_encode([
                'code'=>0,
                'msg'=>'保存成功',
                'data'=>$res
            ]);
        }
        else
        {
            return json_encode([
                'code'=>1,
                'msg'=>'保存失败',
                'data'=>false
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
