<?php

namespace App\Http\Controllers;

use App\FootMenu;
use Illuminate\Http\Request;

class FoodMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

	    $page = $request->input('page', 1);

	    $catid = $request->input('catid', 1);
	    $pageSize = 5;
	    $offset = ($page - 1) * $pageSize;
	    $data['data'] = FootMenu::where(['catid'=>$catid])->offset($offset)->limit($pageSize)->get();
	    $data['total_page'] = FootMenu::where([])->count();
	    if($data) {
		    return [
			    'code'    => 200,
			    'message' => 'ok',
			    'data'    => $data
		    ];
	    } else {
		    return [
			    'code'    => 200,
			    'message' => 'ok',
			    'data'    => []
		    ];
	    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
	    $validator = Validator::make($request->all(), [
		    'title' => 'required|unique:baike'
	    ]);
	    if($validator->fails()) {
		    return $this->jsonRes(0, "请检查传入参数");
	    }
	    //验证重复
	    $foot = FootMenu::where('title', '=', $request->input('title'))->get(array('id'))->toArray();
	    if(!empty($BaikeData)) {
		    return $this->jsonRes(0, "不能重复");
	    }

	    $footMenuDatas = FootMenu::create($request->all());
	    return $this->jsonRes(1, $footMenuDatas);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FootMenu  $footMenu
     * @return \Illuminate\Http\Response
     */
    public function show(FootMenu $footMenu)
    {
        //
	    return $this->jsonRes(1, $footMenu);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FootMenu  $footMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(FootMenu $footMenu)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FootMenu  $footMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FootMenu $footMenu)
    {
        //
	    $footMenu->update($request->all());
	    return $this->jsonRes(1, $footMenu);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FootMenu  $footMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootMenu $footMenu)
    {
        //
    }
}
