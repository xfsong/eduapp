<?php

namespace App\Http\Controllers;

use App\FootCat;
use Illuminate\Http\Request;

class FoodCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
	    $page = $request->input('page', 1);
	    $pageSize = 5;
	    $offset = ($page - 1) * $pageSize;
	    $data['data'] = FootCat::where([])->offset($offset)->limit($pageSize)->get();
	    $data['total_page'] = FootCat::where([])->count();
	    if($data) {
		    return [
			    'code'    => 200,
			    'message' => 'ok',
			    'data'    => $data
		    ];
	    } else {
		    return [
			    'code'    => 200,
			    'message' => 'ok',
			    'data'    => []
		    ];
	    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

	    $validator = Validator::make($request->all(), [
		    'title' => 'required|unique:footcat'
	    ]);
	    if($validator->fails()) {
		    return $this->jsonRes(0, "请检查传入参数");
	    }
	    //验证重复
	    $footCatData = FootCat::where('title', '=', $request->input('title'))->get(array('id'))->toArray();
	    if(!empty($footCatData)) {
		    return $this->jsonRes(0, "不能重复");
	    }

	    $foodCat = FootCat::create($request->all());
	    return $this->jsonRes(1, $foodCat);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FootCat  $footCat
     * @return \Illuminate\Http\Response
     */
    public function show(FootCat $footCat)
    {
        //
	    return $this->jsonRes(1, $footCat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FootCat  $footCat
     * @return \Illuminate\Http\Response
     */
    public function edit(FootCat $footCat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FootCat  $footCat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FootCat $footCat)
    {
        //
	    $footCat->update($request->all());
	    return $this->jsonRes(1, $footCat);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FootCat  $footCat
     * @return \Illuminate\Http\Response
     */
    public function destroy(FootCat $footCat)
    {
        //
    }
}
