<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grow_records extends Model
{
     protected $table='grow_records';
    protected $fillable=['memid','babyid','cretime','body_tall','birthday','body_heavy'];
}
