<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Collect extends Model
{
    protected $table = 'collect';

    protected $fillable = [
        'title', 'photo','info','num','sum',
    ];
}