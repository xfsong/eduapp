<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['cors'])->group(function () {
	Route::apiResource('/babys', 'BabyController')->middleware(Cors::class);//宝宝接口
	Route::apiResource('/baike', 'BaikeController')->middleware(Cors::class);//百科接口
	Route::apiResource('/foodcat', 'FoodCatController')->middleware(Cors::class);//菜谱分类接口
	Route::apiResource('/foodmenu', 'FoodMenuController')->middleware(Cors::class);//菜谱接口
	Route::apiResource('/baikecat', 'BaikeCatController')->middleware(Cors::class);//百科分类接口
	Route::apiResource('/members', 'MemberController')->middleware(Cors::class);//会员接口
	Route::apiResource('/show', 'CollectController')->middleware(Cors::class);//收藏展示接口
	Route::apiResource('/info', 'CollectController')->middleware(Cors::class);//收藏详情接口
	Route::apiResource('login', 'CollectController')->middleware(Cors::class);//登录接口
});
Route::resource('czjl','CzjlController');




Route::resource('baby','BabyColltroller')->middleware(Cors::class);//宝宝信息修改

Route::resource('babys','BabysController')->middleware(Cors::class);//宝宝昵称修改
Route::any('growEdit','GrowController@growEdit')->middleware(Cors::class);//成长记录添加
Route::any('growShow','GrowController@growShow')->middleware(Cors::class);//成长记录展示
