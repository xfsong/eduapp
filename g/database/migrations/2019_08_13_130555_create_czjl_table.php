<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCzjlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('czjl', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cm',20)->comment('身高');
            $table->string('kg',20)->comment('体重');
            $table->string('date',20)->comment('日期');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('czjl');
    }
}
