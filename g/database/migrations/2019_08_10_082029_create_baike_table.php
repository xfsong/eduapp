<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baike', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer("cat_id")->nullable(false)->default(0);
	        $table->string('title')->nullable(false)->default('');
	        $table->string('logos',500)->nullable(false)->default('');
	        $table->text('content');
	        $table->tinyInteger('status')->nullable(false)->default(0);
	        $table->integer("clicks")->nullable(false)->default(0);
	        $table->integer("likes")->nullable(false)->default(0);

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baike');
    }
}
