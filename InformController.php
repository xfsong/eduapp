<?php

namespace App\Http\Controllers;

use App\Inform;
use Illuminate\Http\Request;

class InformController extends Controller
{
    //

    public  function formjson($code,$msg,$data){
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:*');
        header('Access-Control-Allow-Headers:*');
        header('Access-Control-Allow-Credentials:false');

        return json_encode(['code'=>$code,'msg'=>$msg,'data'=>$data]);
    }
    public function index(){
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:*');
        header('Access-Control-Allow-Headers:*');
        header('Access-Control-Allow-Credentials:false');

        $data=Inform::get();
        return $this->formjson(200,'请求成功',$data);
    }
    public function show(){
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:*');
        header('Access-Control-Allow-Headers:*');
        header('Access-Control-Allow-Credentials:false');

        $id=$_GET['id'];
        $data=Inform::findorfail($id);
        return $this->formjson(200,'请求成功',$data);
    }
}
