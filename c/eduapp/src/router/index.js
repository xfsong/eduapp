import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import addjl from '@/components/addjl'
import show from '@/components/show'
import show1 from '@/components/show1'
import baby from '@/components/baby'
import add_info from '@/components/add_info'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/addjl',
      name: 'addjl',
      component: addjl
    },
    {
      path: '/show',
      name: 'show',
      component: show
    },
    {
      path: '/show1',
      name: 'show1',
      component: show1
    },
    {
      path: '/baby',
      name: 'baby',
      component: baby
    },
    {
      path: '/add_info',
      name: 'add_info',
      component: add_info
    }
  ]
})
