import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Bn from '@/components/Bn'
import Zj from '@/components/Zj'
import Dq from '@/components/Dq'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Bn',
      name: 'Bn',
      component: Bn
    },
    {
      path: '/Zj',
      name: 'Zj',
      component: Zj
    },
    {
      path: '/Dq',
      name: 'Dq',
      component: Dq
    }
  ]
})
